
deploy:
	docker-compose run --rm rancher-deploy up --stack $(RANCHER_STACK) --interval 120 --batch-size 1 -d

confirm:
	docker-compose run --rm rancher-deploy up --stack $(RANCHER_STACK) --confirm-upgrade -d

rollback:
	docker-compose run --rm rancher-deploy up --stack $(RANCHER_STACK) --rollback -d

run:
	docker-compose run --rm app
